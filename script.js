const product = {
    name: "Milk",
    price: 10,
    discount: 0.9,
}

function fullPrice() {
        let priceWithout = product.price * product.discount;
        return priceWithout;
}

console.log(`Повна ціна: ${fullPrice()}`);


// Cloning (3-rd assignment)


function recCopy(object) {

    if (typeof object !== 'object' || object === null) {
        return object;
    }

    let copy;
    if (Array.isArray(object)) {
        copy = [];
    } else {
        copy = {};
    }

    for (let key in object) {
        copy[key] = recCopy(object[key]);
    }

    return copy;
}

let prodCopy = recCopy(product);
console.log(prodCopy);


/////////////////////////////////////////////////////////////////////////////////


function greeting(name, age) {
    if (isNaN(age)) {
        alert("Введено не число.");
    } else {
        return alert(`Доброго дня ${name}, вам ${age} років.`);
    }
}

greeting(prompt("name?"), prompt("age?"))


/////////////////////////////////////////////////////////////////////////////////